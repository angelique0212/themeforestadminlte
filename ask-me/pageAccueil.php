
<?php
include 'config.php';
include 'header.php';
?>	
<section class="container main-content">
			<div class="row">
				<div class="col-md-9">
					<?php
                        $req = $pdo->prepare("SELECT id,titre,extrait,date_creation,photo FROM article ORDER BY titre"); 
                        $req->execute();
                        $results = $req->fetchAll();
                        foreach($results as $article) { ?>
                            <article class="post clearfix">
                                <div class="post-inner">
									<div class="post-img"><a href="artcle.php?id=<?php echo $article["id"] ?>"><img src="<?php echo $article['photo'] ?>" alt=""></a>
									
                                    <div class="post-content">
                                        <p><?php echo $article['extrait']; ?></p>
                                        <a href="article.php?id=<?php echo $article["id"] ?>" class="post-read-more button color small">Continuer la lecture</a>
                                    </div><!-- End post-content -->
                                </div><!-- End post-inner -->
                            </article><!-- End article.post -->
                        <?php } ?>	
					
					<div class="pagination">
						
						<a href="#" class="prev-button"><i class="icon-angle-left"></i></a>
						<span class="current">1</span>
						<a href="#" class="next-button"><i class="icon-angle-right"></i></a>
					
				</div><!-- End main -->
				<!-- End sidebar -->
			</div><!-- End row -->
</section><!-- End container -->
		
	
		<?php
		include 'footer.php';
		?>