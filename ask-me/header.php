<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

	<!-- Basic Page Needs -->
	<meta charset="utf-8">
	<title>Ask me – Responsive Questions and Answers Template</title>
	<meta name="description" content="Ask me Responsive Questions and Answers Template">
	<meta name="author" content="vbegy">
	
	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- Main Style -->
	<link rel="stylesheet" href="style.css">
	
	<!-- Skins -->
	<link rel="stylesheet" href="css/skins/skins.css">
	
	<!-- Responsive Style -->
	<link rel="stylesheet" href="css/responsive.css">
	
	<!-- Favicons -->
	<link rel="shortcut icon" href="images/favicon.png">
  
</head>
<body>



	<div class="loader" style="display: none;"><div class="loader_html"></div></div>

	<div id="wrap" class="grid_1200">
		
		<div class="login-panel" style="display: none;">
			<section class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="page-content">
							<h2>Login</h2>
							<div class="form-style form-style-3">
								<form>
									<div class="form-inputs clearfix">
										<p class="login-text">
											<input type="text" value="Username" onfocus="if (this.value == 'Username') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Username';}">
											<i class="icon-user"></i>
										</p>
										<p class="login-password">
											<input type="password" value="Password" onfocus="if (this.value == 'Password') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Password';}">
											<i class="icon-lock"></i>
											<a href="#">Forget</a>
										</p>
									</div>
									<p class="form-submit login-submit">
										<input type="submit" value="Log in" class="button color small login-submit submit">
									</p>
									<div class="rememberme">
										<label><input type="checkbox" checked="checked"> Remember Me</label>
									</div>
								</form>
							</div>
						</div><!-- End page-content -->
					</div><!-- End col-md-6 -->
					<div class="col-md-6">
						<div class="page-content Register">
							<h2>Register Now</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi adipiscing gravdio, sit amet suscipit risus ultrices eu. Fusce viverra neque at purus laoreet consequa. Vivamus vulputate posuere nisl quis consequat.</p>
							<a class="button color small signup">Create an account</a>
						</div><!-- End page-content -->
					</div><!-- End col-md-6 -->
				</div>
			</section>
		</div><!-- End login-panel -->
		
		<div class="panel-pop" id="signup" style="margin-top: -198px;">
			<h2>Register Now<i class="icon-remove"></i></h2>
			<div class="form-style form-style-3">
				<form>
					<div class="form-inputs clearfix">
						<p>
							<label class="required">Username<span>*</span></label>
							<input type="text">
						</p>
						<p>
							<label class="required">E-Mail<span>*</span></label>
							<input type="email">
						</p>
						<p>
							<label class="required">Password<span>*</span></label>
							<input type="password" value="">
						</p>
						<p>
							<label class="required">Confirm Password<span>*</span></label>
							<input type="password" value="">
						</p>
					</div>
					<p class="form-submit">
						<input type="submit" value="Signup" class="button color small submit">
					</p>
				</form>
			</div>
		</div><!-- End signup -->
		
		<div class="panel-pop" id="lost-password" style="margin-top: -157.5px;">
			<h2>Lost Password<i class="icon-remove"></i></h2>
			<div class="form-style form-style-3">
				<p>Lost your password? Please enter your username and email address. You will receive a link to create a new password via email.</p>
				<form>
					<div class="form-inputs clearfix">
						<p>
							<label class="required">Username<span>*</span></label>
							<input type="text">
						</p>
						<p>
							<label class="required">E-Mail<span>*</span></label>
							<input type="email">
						</p>
					</div>
					<p class="form-submit">
						<input type="submit" value="Reset" class="button color small submit">
					</p>
				</form>
				<div class="clearfix"></div>
			</div>
		</div><!-- End lost-password -->
		
		<!-- End header-top -->
		<header id="header">
	
		<section class="container clearfix">
			
				
		<nav class="navigation">
					<ul>
						
						<li class="current_page_item parent-list"><a href="pageAccueil.php">Accueil<span class="menu-nav-arrow"></span></a>
							
						</li>
						<li class="parent-list"><a href="AdminLTE/ajoutArticle.php">Ajouter un article<span class="menu-nav-arrow"></span></a>
							</span> <span class="menu-nav-arrow"><i class="icon-angle-down"></i></span></a><ul style="overflow: hidden; height: 35px; padding-top: 0px; margin-top: 0px; padding-bottom: 0px; margin-bottom: 0px; display: none;"><a href="#">
							
								</a><li><a href="#"></a><a href="single_post.html">Article</a></li>	
							</ul>
						</li>
						
						
					</ul>
				</nav>
			
			</section><!-- End container -->
		</header><!-- End header -->